<?php

require_once("animal.php");
require_once("ape.php");
require_once("frog.php");

$sheep = new Animal("shaun");

echo "Name : " . $sheep->name . "<br>"; // "shaun"
echo "Legs : " . $sheep->legs . "<br>"; // 4
echo "cold blodeed : " . $sheep->cold_blooded . "<br> <br>"; // "no"

$ape = new Ape("kera sakti");

echo "Name : " . $ape->name . "<br>"; // "kara sakti"
echo "Legs : " . $ape->legs . "<br>"; // 4
echo "cold blodeed : " . $ape->cold_blooded . "<br>"; // "no"
echo $ape->yell() . "<br>";

$frog = new Frog("Buduk");

echo "Name : " . $frog->name . "<br>"; // "Buduk"
echo "Legs : " . $frog->legs . "<br>"; // 2
echo "cold blodeed : " . $frog->cold_blooded . "<br>"; // "no"
echo $frog->jump();


?>