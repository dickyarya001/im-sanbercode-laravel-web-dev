<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function reg()
    {
        return view('halaman.form');
    }

    public function send(Request $request)
    {
        $namaDepan = $request['firstname'];
        $namaBelakang = $request['lastname'];

        return view('halaman.welcome' , ['firstname' => $namaDepan, 'lastname' => $namaBelakang]);
    }
}
