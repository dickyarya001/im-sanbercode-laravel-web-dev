@extends('layout.master')

@section('title')
    Halaman Detail Cast
@endsection
@section('sub-title')
    cast
@endsection

@section('content')
<h1>{{$cast->name}}</h1>
<p>{{$cast->umur}}</p>
<p>{{$cast->bio}}</p>

<a href="/cast" class="btn btn-secondary">Back</a>

@endsection