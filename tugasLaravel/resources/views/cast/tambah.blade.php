@extends('layout.master')

@section('title')
    Halaman Tambah Cast

@endsection
@section('sub-title')
    cast
@endsection

@section('content')
<form action="/cast" method="POST">
  @csrf
  <div class="form-group">
    <label>Cast Name</label></label>
    <input type="text" class="form-control @error('name') is-invalid @enderror" name="name">
  </div>
  @error('name')
    <div class="alert alert-danger">{{ $message }}</div>
@enderror
  <div class="form-group">
    <label>Cast Umur</label>
    <input type="text" class="form-control @error('name') is-invalid @enderror" name="umur">
  </div>
  @error('umur')
    <div class="alert alert-danger">{{ $message }}</div>
@enderror
  <div class="form-group">
    <label>Cast Bio</label>
    <textarea name="bio" class="form-control  @error('name') is-invalid @enderror"></textarea>
  </div>
  @error('bio')
    <div class="alert alert-danger">{{ $message }}</div>
@enderror

    <button type="submit" class="btn btn-primary">Submit</button>
</form>
   
@endsection