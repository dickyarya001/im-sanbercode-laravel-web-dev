@extends('layout.master')

@section('title')
    Halaman Tambah Cast
@endsection
@section('sub-title')
    cast
@endsection

@section('content')

<a href="/cast/create" class="btn btn-primary btn-sm my-3">Tambah</a>

<table class="table">
  <thead>
    <tr>
      <th scope="col">#</th>
      <th scope="col">name</th>
      <th scope="col">umur</th>
      <th scope="col">bio</th>
    </tr>
  </thead>
  <body>
    @forelse ($cast as $key => $item)
      <tr>
        <td>{{$key + 1}}</td>
        <td>{{$item->name}}</td>
        <td>
          
          <from action="/cast/{{$item->id}}" method="post">
            @method('delete')
            @csrf
            <a href="/cast/{{$item->id}}" class="btn btn-info btn-sm">Detail</a>
            <a href="cast/{{$item->id}}/edit" class="btn btn-warning">Edit</a>
            <input type="submit" class="btn btn-danger btn-sm" value="Delete">
          </from>
        </td>
        <td>
          <from action="/cast/{{$item->id}}" method="post">
            @method('delete')
            @csrf
            <a href="/cast/{{$item->id}}" class="btn btn-info btn-sm">Detail</a>
            <a href="cast/{{$item->id}}/edit" class="btn btn-warning">Edit</a>
            <input type="submit" class="btn btn-danger btn-sm" value="Delete">
          </from>
        </td>
      </tr>
        
    @empty
      <tr>
        <td>Data Kosong</td>
      </tr>
    @endforelse
  </body>
</table>

@endsection