@extends('layout.master')

@section('title')
  Halaman Pendaftaran
@endsection
@section('sub-title')
  Pendaftaran
@endsection
@section('content')
<h1>Buat Account Baru</h1>
<h2>Sign Up Form</h2>
    <form action="/kirim" method="post">
      @csrf
      <label>First Name</label> <br />
      <input type="text" name="firstname" /> <br />
      <br />
      <label>Last Name</label> <br />
      <input type="text" name="lastname" /> <br />
      <br />
      <label>Gender</label> <br />
      <input type="radio" name="Gender" />Male <br />
      <input type="radio" name="Gender" />Femele <br />
      <input type="radio" name="Gender" />Other <br />
      <br />
      <label>Nationality</label>
      <select name="Nationality">
        <option value="">Indonesia</option>
        <option value="">Jepang</option>
        <option value="">Inggris</option>
        <option value="">Rusia</option>
      </select>
      <br />
      <br />
      <label>Language Spoken</label> <br />
      <input type="checkbox" name="" />Bahasa Indonesia<br />
      <input type="checkbox" name="" />English<br />
      <input type="checkbox" name="" />Other<br />
      <label>Bio</label> <br />
      <textarea cols="30" rows="10"></textarea> <br />
      <br />

      <input type="submit" value="kirim" />
    </form>
@endsection

  
