@extends('layout.master')

@section('title')
    Halaman Selamat Datang

@endsection
@section('sub-title')
    Selamat Datang
@endsection

@section('content')
    <h1>Selamat Datang {{$firstname}} {{$lastname}}</h1>
    <h2>Terimakasih telah bergabung di Sanberbook. Social media kita bersama</h2>
@endsection