<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\CastController;
use PhpParser\Node\Expr\Cast;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [HomeController::class, 'utama']);
Route::get('/register', [AuthController::class, 'reg']);

Route::post('/kirim', [AuthController::class, "send"]); //post buat ngirim data k tampilan yg mau d tuju

Route::get('/data-table', function(){ //ngambil data atau nampilin
  return view('halaman.datatable');
});

Route::get('/table', function(){
  return view('halaman.tabel');
});

//CRUD cast

//Crate Data
//Route   untuk mengarah ke form tambah cast
Route::get('/cast/create',[CastController::class, 'create']);

//Route untuk menyimpan data inputan ke table cast database
Route::post('/cast', [CastController::class, 'store']);

//Read cast

//Route untuk tampil semua data di table cast DB
Route::get('/cast', [CastController::class, 'index']);

//Route untuk detail berdasarkan id
Route::get('/cast/{id}', [CastController::class, 'show']);

//Route cast

//Rouute untuk mengarah ke form edit cast
Route::get('/cast/{id}/edit', [CastController::class, 'edit']);

//Route untuk update data bedasarkan id table DB
Route::put('/cast/{id}',  [CastController::class, 'update']);

//delete cast
//Route untuk delete cast bedasarkan id nya

Route::delete('/cast/{id}', [CastController::class, 'destroy']);